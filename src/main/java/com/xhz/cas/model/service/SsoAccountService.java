package com.xhz.cas.model.service;

import com.xhz.cas.model.entity.SsoAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账户表 单点登录账号信息表 服务类
 * </p>
 *
 * @author huangcb
 * @since 2020-04-17
 */
public interface SsoAccountService extends IService<SsoAccount> {

    SsoAccount getByLoginInfo(String info);
}
