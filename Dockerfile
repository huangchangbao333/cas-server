FROM java:8

VOLUME /tmp

ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY ./target/server-sso-0.0.1-SNAPSHOT.jar /opt/server-sso-0.0.1-SNAPSHOT.jar
RUN bash -c 'touch /opt/server-sso-0.0.1-SNAPSHOT.jar'

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "-Xms1024m", "-Xmx1024m", "-XX:MetaspaceSize=124m", "-XX:MaxMetaspaceSize=224M"  ,"/opt/server-sso-0.0.1-SNAPSHOT.jar", "--spring.profiles.active=test","--server.port=8080", "> /log/server-sso.log"]
