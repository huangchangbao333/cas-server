package com.xhz.cas.model.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xhz.cas.model.entity.SsoAccount;
import com.xhz.cas.model.dao.SsoAccountDao;
import com.xhz.cas.model.service.SsoAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账户表 单点登录账号信息表 服务实现类
 * </p>
 *
 * @author huangcb
 * @since 2020-04-17
 */
@Service
public class SsoAccountServiceImpl extends ServiceImpl<SsoAccountDao, SsoAccount> implements SsoAccountService {

    @Override
    public SsoAccount getByLoginInfo(String info) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account",info);
        queryWrapper.or(true).eq("email",info);
        queryWrapper.or(true).eq("phone_num",info);
        return getOne(queryWrapper);
    }
}
