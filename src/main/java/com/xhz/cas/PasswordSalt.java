package com.xhz.cas;

import com.xhz.cas.tools.PasswordEncryptUtil;
import org.apache.shiro.crypto.hash.ConfigurableHashService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.util.ByteSource;

/**
 * 加盐密码生成
 *
 * @author huangcb on 2020/1/15
 */
public class PasswordSalt {
    private static String encodedPassword = "123456";
    private static String dynaSalt = "admin";

    public static void main(String[] args) {
        String res = PasswordEncryptUtil.encrypt(encodedPassword,dynaSalt);
        System.out.println(res);
    }
}
