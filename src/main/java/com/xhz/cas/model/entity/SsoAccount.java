package com.xhz.cas.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import lombok.Data;

/**
 * <p>
 * 账户表 单点登录账号信息表
 * </p>
 *
 * @author huangcb
 * @since 2020-04-17
 */
@Data
public class SsoAccount extends Model<SsoAccount> {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 过期标识 ，1为过期，若过期不可用
     */
    private int expired;

    /**
     * 不可用 为不可用字段，1为不可用，需要修改密码
     */
    private int disabled;

    /**
     * 创建人
     */
    private String createdUser;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SsoAccount{" +
                "id=" + id +
                ", account=" + account +
                ", password=" + password +
                ", expired=" + expired +
                ", disabled=" + disabled +
                ", createdUser=" + createdUser +
                ", createdTime=" + createdTime +
                ", updatedUser=" + updatedUser +
                ", updateTime=" + updateTime +
                ", name=" + name +
                ", phone=" + phone +
                ", email=" + email +
                "}";
    }
}
