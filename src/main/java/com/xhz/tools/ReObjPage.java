package com.xhz.tools;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhz.eunm.ReModelEnum;

import java.util.List;

public class ReObjPage<T>{
	private String code;
	private String msg;
	// 当前页
    private int page = 1;
    // 每页显示的总条数
    private Integer limit = 10;
    // 总条数
    private long count;
    // 是否有下一页
    private Integer isMore;
    // 总页数
    private long totalPage;
    // 开始索引
    private Integer startIndex;
    // 分页结果
    private List<T> data;

    public ReObjPage() {
        super();
    }
    
  //使用枚举类型更利于维护
  	public ReObjPage (ReModelEnum reModelEnum) {
  		this.code = reModelEnum.getValue();
  	}
  	
  	public ReObjPage (ReModelEnum reModelEnum, String msg) {
  		this.code = reModelEnum.getValue();
  		this.msg = msg;
  	}
  	
  	public ReObjPage (ReModelEnum reModelEnum, String msg , List<T> data) {
  		this.code = reModelEnum.getValue();
  		this.msg = msg;
  		this.data = data;
  	}
  	
  	public ReObjPage (ReModelEnum reModelEnum, List<T> data) {
  		this.code = reModelEnum.getValue();
  		this.data = data;
  	}

  	public Page<T> buildPagination() {
  		Page<T> pageT = new Page<T>();
  		pageT.setCurrent(page);
  		pageT.setSize(limit);
  		return pageT;
  	}
  	
  	public ReObjPage<T> build(Page<T> page) {
  		this.data = page.getRecords();
  		this.count = page.getTotal();
  		this.totalPage = page.getPages();
  		setCode(ReModelEnum.SUCCESS.getValue());
  		return this;
  	}
  	
	public int getLimit() {
		return limit;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public Integer getIsMore() {
        return isMore;
    }

    public void setIsMore(Integer isMore) {
        this.isMore = isMore;
    }

    public long getTotalPage() {
        return totalPage;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
