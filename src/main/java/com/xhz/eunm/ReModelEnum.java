package com.xhz.eunm;

/**
 * 返回对象参数枚举类
 * @author huangcb
 * @since 2018-11-20
 */
public enum ReModelEnum {
	
	//成功
	SUCCESS("0",0),	
	//失败
	FAILD("-1",-1);		
	
	String value;
	int integerValue;
	private ReModelEnum(String value,int integerValue) {
		this.value = value;
		this.integerValue = integerValue;
	}
	
	public String getValue() {
		return value;
	}

	public int getIntegerValue() {
		return integerValue;
	}
}
