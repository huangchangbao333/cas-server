package com.xhz.cas.tools;

import org.apache.shiro.crypto.hash.ConfigurableHashService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;

/**
 * 密码加密工具类
 *
 * @author huangcb on 2020/4/21
 */
public class PasswordEncryptUtil {
    private static final String STATIC_PSALT = "123";

    private static final int ITERATION = 2;

    public static String encrypt(String encodedPassword, String dynaSalt) {
        return encrypt(encodedPassword, dynaSalt, ITERATION);
    }

    public static String encrypt(String encodedPassword, String dynaSalt, int iteration) {
        ConfigurableHashService hashService = new DefaultHashService();
        hashService.setPrivateSalt(ByteSource.Util.bytes(STATIC_PSALT));
        hashService.setHashAlgorithmName(Md5Hash.ALGORITHM_NAME);
        hashService.setHashIterations(iteration);
        HashRequest request = new HashRequest.Builder()
                .setSalt(dynaSalt)
                .setSource(encodedPassword)
                .build();
        String res = hashService.computeHash(request).toHex();
        return res;
    }
}
