package com.xhz.cas.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 扫描配置类
 *
 * @author huangcb on 2020/4/17
 */
@Configuration
@ComponentScan({"org.apereo.cas","com.xhz.cas"})
public class ComponentScanConfig {
}
