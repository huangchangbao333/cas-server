package com.xhz.cas.model.dao;

import com.xhz.cas.model.entity.SsoAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账户表 单点登录账号信息表 Mapper 接口
 * </p>
 *
 * @author huangcb
 * @since 2020-04-17
 */
public interface SsoAccountDao extends BaseMapper<SsoAccount> {

}
