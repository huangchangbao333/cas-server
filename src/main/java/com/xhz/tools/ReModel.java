package com.xhz.tools;

import com.xhz.eunm.ReModelEnum;

public class ReModel {

	private String code;//0-成功
	private String msg;
	private Object obj;
	
	//使用枚举类型更利于维护
	public ReModel (ReModelEnum reModelEnum) {
		this.code = reModelEnum.getValue();
	}
	
	public ReModel (ReModelEnum reModelEnum, String msg) {
		this.code = reModelEnum.getValue();
		this.msg = msg;
	}
	
	public ReModel (ReModelEnum reModelEnum, String msg , Object obj) {
		this.code = reModelEnum.getValue();
		this.msg = msg;
		this.obj = obj;
	}
	
	public ReModel (ReModelEnum reModelEnum, Object obj) {
		this.code = reModelEnum.getValue();
		this.obj = obj;
	}

	
	/*不建议使用*/
	@Deprecated
	public ReModel(String code,String msg,Object obj) {
		this.code=code;
		this.msg=msg;
		this.obj=obj;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}

	@Override
	public String toString() {
		return "[code:" + code + ", msg:" + msg + ", obj:" + obj + "]";
	}
	
	
}
