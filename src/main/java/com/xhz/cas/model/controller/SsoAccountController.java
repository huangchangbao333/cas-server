package com.xhz.cas.model.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 账户表 单点登录账号信息表 前端控制器
 * </p>
 *
 * @author huangcb
 * @since 2020-04-17
 */
@RestController
@RequestMapping("/cas.model/ssoAccount")
public class SsoAccountController {

}

